import jsonServer from 'json-server'
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()
const port = process.env.PORT || 3000

server.use(middlewares)


server.get('/get503error', (req, res) => {
  console.log('/get503error, error 503')
  res.status(503).jsonp({
    error: "Service Unavailable"
  })
})
server.post('/get503error', (req, res) => {
  console.log('/get503error, error 503')
  res.status(503).jsonp({
    error: "Service Unavailable"
  })
})
server.use(jsonServer.bodyParser)
server.use((req, res, next) => {
  if (req.method === 'POST') {
    req.body.createdAt = Date.now()
  }
  next()
})

server.use(router)

server.listen(port, () => {
  console.log('JSON Server is running on http://localhost:' + port)
})