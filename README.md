# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

During Cyverse maintenance period (usually a day), BioViz Connect will be down. During the maintenance, Cyverse opens up their individual services randomly and that effects the BioViz Connect user experience. Error handlers are included in the Bioviz Connect to improve the experience during such a time. However, in order to test the error handlers we wanted to simulate the errors using this test server.

### How do I get set up? ###

 * Clone the repository in your local system
 * Open terminal in \bvc_error_simulator_server\bvc_error_simulator_API\
 * Install node.js and npm 
 * Install typescript using "npm install typescript"
 * Install yarn using "npm install yarn"
 * Use the command "yarn start" to start the server
 * If no errors appear in a browser, go to http://localhost:3000
 * If "Congrats!" page appears the server is running fine.
 * Go to http://localhost:3000/get503error to get "Service unavailable" error
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact